<?php

namespace Drupal\killadblock\Form;

/**
 * @file
 * Contains Drupal\killadblock\Form\KillAdBlock.
 */

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\RoleStorageInterface;

/**
 * Class KillAdBlock.
 */
class KillAdBlock extends ConfigFormBase {

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected $roleStorage;

  /**
   * Constructs an EuCookieComplianceConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\user\RoleStorageInterface $role_storage
   *   The role storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RoleStorageInterface $role_storage) {
    parent::__construct($config_factory);

    $this->roleStorage = $role_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity.manager')->getStorage('user_role')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'killadblock.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'killadblock_form';
  }

  /**
   * Gets the roles to display in this form.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of role objects.
   */
  protected function getRoles() {
    return $this->roleStorage->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('killadblock.settings');

    $form['kadb_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Enter title for visitiors'),
      '#default_value' => $config->get('kadb_title'),
    ];

    $form['kadb_description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Enter Description  or reason why user whitelist your website  you can use html add logo or hardcode html'),
      '#default_value' => $config->get('kadb_description.value'),
    ];

    $form['kadb_btn_txt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Text'),
      '#description' => $this->t('Add message to the button'),
      '#default_value' => $config->get('kadb_btn_txt'),
    ];

    $defaultRoles = $config->get('roles');
    $options = [];
    foreach ($this->getRoles() as $role_name => $role) {
      $options[$role_name] = $role->label();
    }

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select roles to show Adblock popup'),
      '#options' => $options,
      '#default_value' => isset($defaultRoles) ? $defaultRoles : FALSE,
    ];

    $form['cache'] = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('killadblock.settings');
    $config->set('kadb_title', $form_state->getValue('kadb_title'))
      ->set('kadb_description.value', $form_state->getValue('kadb_description')['value'])
      ->set('kadb_btn_txt', $form_state->getValue('kadb_btn_txt'))
      ->set('roles', $form_state->getValue('roles'))
      ->save();
  }

}
