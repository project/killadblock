CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Kill ad block helps you to detects if a user is using ad blocker plugin like
Adblock plus, Adblock, uBlock Adblocker Plus on the following web browsers
and display a banner to disable the ad block or white-list your website.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/killadblock

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/killadblock


REQUIREMENTS
------------

This module doesn't require any module outside of Drupal core.


INSTALLATION
------------

 * Install this module as you would normally install a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Add your own custom message or html template at /admin/config/killadblock.


MAINTAINERS
-----------

 * Gaurav Kapoor - https://www.drupal.org/u/gauravkapoor
 * Tanuj Sharma - https://www.drupal.org/u/badmetevils

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
